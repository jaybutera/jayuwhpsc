
"""
Demonstration module for quadratic interpolation.
Update this docstring to describe your code.
Modified by: ** your name here **
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:

    A = np.vstack([np.ones(3), xi, xi**2]).T

    c = solve(A,yi)

    return c

def plot_quad(xi, yi):
    """
    Plot a quadratic interpolation. Plot yi/xi.

    """

    # Perform quadratic interpolation
    c = quad_interp(xi,yi)

    # Plot
    x = np.linspace(xi.min()-1, xi.max()+1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2

    plt.figure(1)
    plt.clf()
    plt.plot(x,y,'b-')
    plt.plot(xi,yi,'ro')
    plt.title('Quadratic Interpolation')

    plt.savefig('quadratic.png')
    print 'Saved quadratic interpolation plot'

def cubic_interp(xi, yi):
    """
    Cubic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2,3.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3.

    """

    # Validate input data
    error_message = "Both xy and yi must consist of np.ndarray types"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==4 and len(yi)==4, error_message

    # Solve linear system
    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T
    c = solve(A,yi)

    return c

def plot_cubic(xi, yi):
    """
    Plot a quadratic interpolation. Plot yi/xi.

    """
    # Create data points to graph
    c = cubic_interp(xi, yi)

    x = np.linspace(xi.min()-1, xi.max()+1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3

    # Plot points
    plt.figure(1)
    plt.clf()
    plt.plot(x,y,'b-')
    plt.plot(xi,yi,'ro')
    plt.title('Cubic Interpolation')

    plt.savefig('cubic.png')
    print 'Saved cubic interpolation plot'

def poly_interp(xi,yi):
    """
    Polynomial interpolation. Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1...n-1.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x**1 + c[n-1]*(n-1)**(n-1)

    """
    # Validate input data
    error_message = "Both xy and yi must consist of np.ndarray types"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi vectors must be the same length"
    assert len(xi)==len(yi), error_message

    # Solve linear system
    n = len(xi)
    A = np.vstack([xi**i for i in range(n)]).T
    c = solve(A,yi)

    return c

def plot_poly(xi,yi):
    """
    Plot a polynomial interpolation. Plot yi/xi.

    """
    c = poly_interp(xi,yi)
    n = len(xi)
    x = np.linspace(xi.min()-1, xi.max()+1, 1000)
    y = c[n-1]
    for i in range(n-1, 0, -1):
        y = y*x + c[i-1]

    # Plot
    plt.figure(1)
    plt.clf()
    plt.plot(x, y, 'b-')
    plt.plot(xi, yi, 'ro')
    plt.title('Polynomial Interpolation')

    plt.savefig('polynomial.png')
    print 'Saved polynomial interpolation plot'

def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_quad2():
    """
    Test code, no return value or exception if test runs properly.
    Tests plot_quad() function.
    """
    xi = np.array([-1., 0., 2.])
    yi = np.array([ 1.,-1., 7.])
    plot_quad(xi,yi)

def test_cubic2():
    """
    Test code, no return value or exception if test runs properly.
    Tests plot_cubic function.
    """
    xi = np.array([-1., 0., 2., 3.])
    yi = np.array([ 1.,-1., 7., 2.])
    plot_cubic(xi,yi)

def test_poly2():
    """
    Test code, no return value or exception if test runs properly.
    Tests plot_poly function.
    """
    xi = np.array([ 4.,-3., 1., 5., 0., 2.])
    yi = np.array([-3., 1., 0., 4., 7.,-1.])

    plot_poly(xi,yi)

if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()
    test_quad2()
    test_cubic2()


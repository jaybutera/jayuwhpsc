def solve(fx,fp, x0, debug=False):
    """
    Return the newtonian square root approximation of f(x).
    """
    from numpy import nan

    kmax = 100
    num = 0
    tol = 1.e-14
    x = x0

    for k in range(kmax):
        num = num + 1
        if debug:
            print "Before iteration %s, s = %20.15f", (k,x0)

        deltax = fx/fp

        x = x - deltax

        if abs(fx) < tol:
            break

    if debug:
        print "After %s iterations, s = %20.15f" % (k+1,x0)

    return x0,num

def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        fx, fp = fvals_sqrt(x0)
        x,iters = solve(fx, fp, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x
